
# Layout Disable

The Layout Disable module provides an admin UI to disable unwanted layouts.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/layout_disable).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/layout_disable).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

No special requirements.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module from extend.
2. Go to configuration -> User Interfce -> Disable layouts,and select the layout you wish to disable.


## Maintainers

- Julian Pustkuchen - [Anybody](https://www.drupal.org/u/anybody)
- Thomas Frobieter - [thomas.frobieter](https://www.drupal.org/u/thomasfrobieter)
- Joshua Sedler - [Grevil](https://www.drupal.org/u/grevil)
